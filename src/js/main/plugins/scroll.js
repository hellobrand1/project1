$(".scroll")
  .on('click', function (e) {
    e.preventDefault()
    const top = $($(this).attr('href')).offset().top || 0
    $('html,body').stop().animate({
      scrollTop: top
    }, 1000)
  })
