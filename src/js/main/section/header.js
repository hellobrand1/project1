const menu = document.querySelector('[data-role="menu"]')
const sandwich_btn = document.querySelector('[data-role="toggle-float-menu"]')
const float_menu = document.querySelector('[data-role="float-menu"]')

window.onscroll = function () {
  const st = this.pageYOffset

  if (st > 400) {
    menu.classList.add('fixed');
  } else {
    menu.classList.remove('fixed');
  }

  if (st > 500) {
    menu.classList.add('show');
  } else {
    menu.classList.remove('show');
  }

  sandwich_btn.classList.remove('active')
  float_menu.classList.remove('show')
}

sandwich_btn.addEventListener('click', function (e) {
  e.preventDefault()
  this.classList.toggle('active')
  if (this.classList.contains('active')) {
    float_menu.classList.add('show')
  } else {
    float_menu.classList.remove('show')
  }
}, false)

const anchor = document.querySelectorAll('[data-role="scroll-to-anchor"]')

anchor.forEach((el) => {
  el.addEventListener('click', (e) => {
    e.preventDefault()
    const href = el.getAttribute('href')
    const selector = href ? el.getAttribute('href').substr(1) : null
    const target = selector ? document.getElementById(selector) : document.body
    if(href) {
      target.scrollIntoView({
        behavior: 'smooth',
        block: 'start'
      })
    }
  })
})
